version: "3.6"
services:
  db:
    image: mysql:8.0.19
    command: '--default-authentication-plugin=mysql_native_password'
    restart: always
    healthcheck:
      test: ["CMD", "mysqladmin", "ping", "-h", "127.0.0.1", "--silent"]
      interval: 3s
      retries: 5
      start_period: 30s
    secrets:
      - db-password
    volumes:
      - db-data:/var/lib/mysql
    networks:
      - backnet
    environment:
      - MYSQL_DATABASE=example
      - MYSQL_ROOT_PASSWORD_FILE=/run/secrets/db-password
    expose:
      - 3306
      - 33060
  backend:
    build: backend
    restart: always
    secrets:
      - db-password
    ports:
      - 5000:5000
    networks:
      - backnet
      - frontnet
    depends_on:
      db:
        condition: service_healthy
  nginx:
    image: nginx:latest
    container_name: webserver_nginx
    volumes:
      - ./nginx/nginx.conf:/etc/nginx/nginx.conf
      - ./nginx/sites-enabled/backend.conf:/etc/nginx/sites-enabled/backend.template
      - ./nginx/ssl/dhparam.pem:/etc/nginx/ssl/dhparam.pem
      - ./nginx/certs/dev.reciperi.com.pem:/etc/nginx/dev.reciperi.com.pem # New Line!
      - ./nginx/certs/dev.reciperi.com-key.pem:/etc/nginx/dev.reciperi.com-key.pem # New Line!
    ports:
      - 80:80
      - 443:443
    environment:
       - SITE_DOMAIN=test1.ninjcart.com
    networks: 
       - frontnet
    command: /bin/bash -c "envsubst '$${SITE_DOMAIN}'< /etc/nginx/sites-enabled/backend.template > /etc/nginx/sites-enabled/backend.conf && exec nginx -g 'daemon off;'"
volumes:
  db-data:
secrets:
  db-password:
    file: db/password.txt
networks:
  backnet:
  frontnet:
